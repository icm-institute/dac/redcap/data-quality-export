<?php

use phpDocumentor\Reflection\PseudoTypes\False_;
use phpDocumentor\Reflection\PseudoTypes\True_;

header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 14 Mar 1980 20:31:00 GMT");
header('Content-Type: xlsx; charset=utf-8');
header('Content-Disposition: attachment; filename=DataQuality_export.csv');

System::increaseMemory(1024 * 10);
set_time_limit(600);

// Instantiate DataQuality object
$dq = new DataQuality();

$dags = $Proj->getGroups();

$sql = "select record, value from redcap_data where project_id = " . PROJECT_ID . " and field_name = '__GROUPID__'";
$q = db_query($sql);
while ($row = db_fetch_assoc($q))
{
    $this_group_id = $row['value'];
    // Make sure the DAG actually exists (in case was deleted but value remained in redcap_data)
    if (isset($dags[$this_group_id])) {
        $dag_records[$row['record']] = $this_group_id;
    }
}

foreach($_POST['rule'] as $rule_id) {
    $rule_id = htmlspecialchars($rule_id, ENT_QUOTES);

    // Get rule info
    $rule_info = $dq->getRule($rule_id);

    // Execute this rule
    $dq->executeRule($rule_id, NULL);

    $results = $dq->getLogicCheckResults();
    // var_export($results);


    foreach($results[$rule_id] as $result_num => $result_data) {

        $results[$rule_id][$result_num]['rule_name'] = str_replace('"', "", strip_tags(array_values($rule_info)[0]));
        $results[$rule_id][$result_num]['dag_id'] = $dag_records[$result_data['record']];
        $results[$rule_id][$result_num]['dag_name'] = $dags[$dag_records[$result_data['record']]];

        $html = $results[$rule_id][$result_num]['data_display'];
        
        // Supprime les balises HTML et remplace les espaces insécables
        $text = strip_tags(str_replace(array('<br>', '&nbsp;'), array("\n", ' '), $html));
        
        // Supprime les balises HTML et remplace les espaces insécables
        $text = str_replace('&nbsp', '', $text);
        
        // Explode le texte en lignes
        $lines = explode("\n", $text);

        // Filtrer les lignes vides
        $lines = array_filter(array_map('trim', $lines));

        // Initialiser le résultat
        $result = '';

        // Boucler sur les lignes et formater pour le CSV
         foreach ($lines as $line) {
             $result .= '"' . $line . '"' . PHP_EOL;
         }

        // Afficher ou enregistrer le résultat
        $result = str_replace('"', "'", $result);
        $result = str_replace("''", "'", $result);
        $result = preg_replace('/\n$/','',$result);
        $results[$rule_id][$result_num]['data_display'] = $result;
    }

     foreach($results[$rule_id] as $result) {
         if (!$header_printed) {
             echo '"' . implode('";"', array_keys($result)) . '"' . PHP_EOL;
             $header_printed = true;
         }
         echo '"' . implode('";"', $result) . '"' . PHP_EOL;
     }
}

