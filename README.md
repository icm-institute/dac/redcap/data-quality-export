# Data Quality Export

This external module allows you to select multiple data quality reports and export them as CSV

Initial author is Paige Julianne Sullivan at psullivan@jhu.edu

Updated by Mathias Antunes for better CSV reporting.

Source code can be found at https://github.com/metrc/redcap-dq-export